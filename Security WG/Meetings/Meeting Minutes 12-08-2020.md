## Meeting Logistics

| Meeting Day | Meeting Time                    | Conference Bridge                          |
| ----------- | ------------------------------- | ------------------------------------------ |
| 12-08-2020  | 16:30 PM to 17:30 PM (UTC+8:00) | https://welink-meeting.zoom.us/j/          |

## Meeting Attendees
** **
- 
- 



## Meeting Agenda
** **
| Item | Owner |
| ---- | ---- |
| 安全测试方案讨论 | ALL |
| 证书，秘钥，明文数据库密码等社区策略评审 | 陈传雨 |



## Discussion Items and Minutes
** **
- 


## Voting Items
** **
SEC Voting to Approve XYZ:
| No.  | Name | Company | Y/N/A |
| ---- | ---- | ------- | ----- |
| 1    | Name | Company | Y     |

## Action Items
** **
| Done? | Item | Responsible | Due Date |
| ----- | ---- | ----------- | -------- |
|       | item | who         | due_date |

## Other Notes & Information
N/A