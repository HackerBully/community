**********************************
EdgeGallery Security Test Case 002
**********************************

+-----------------------------------------------------------------------------+
| Web Security                                                                |
|                                                                             |
+--------------+--------------------------------------------------------------+
|Test case ID  | EG-TST-SEC-002                                               |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|Test purpose  | The purpose of test case SEC-002 is to perform penetration   |
|              | testing on a web  application and find security              |
|              | vulnerabilities.                                             |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|Test tool     | OWASP Zed Attack Proxy                                       |
|              |                                                              |
|              | Zed Attack Proxy (ZAP) is a free, open-source penetration    |
|              | testing tool being maintained under the umbrella of the Open |
|              | Web Application Security Project (OWASP).                    |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|Test          | ZAP stands between the tester’s browser and the web          |
|description   | application so that it can intercept and inspect messages    |
|              | sent between browser and web application, modify the         |
|              | contents if needed, and then forward those packets on to the |
|              | destination.                                                 |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|Configuration | Test tool and environment setup description                  |
|steps         |                                                              |
+--------------+--------------------------------------------------------------+
|step 1        |                                                              |
|              |                                                              |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|step 2        |                                                              |
|              |                                                              |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|Test          | Test step description and expected result                    |
|steps         |                                                              |
+--------------+--------------------------------------------------------------+
|step 1        |                                                              |
|              |                                                              |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|step 2        |                                                              |
|              |                                                              |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|step 3        |                                                              |
|              |                                                              |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|step 4        |                                                              |
|              |                                                              |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|Test verdict  | Define the result of a test case execution.                  |
|              | It has 5 possible values: none, pass, inconc, fail, error.   |
|              |                                                              |
+--------------+--------------------------------------------------------------+
|References    | Useful external reference URLs.                              |
|              |                                                              |
|              |                                                              |
+--------------+--------------------------------------------------------------+