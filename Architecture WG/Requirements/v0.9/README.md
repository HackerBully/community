> Working In Progress

### EdgeGallery v0.9 Requirements

|Requirements   | EPIC Link  | Requirement Design Doc| Contactor |
|---|---|---|---|
| Network isolation/网络平面隔离  |  [Network_isolation_EPIC](https://gitee.com/OSDT/dashboard/programs/114633/issues?issue_id=I1OY0L) | [network isolation design doc](https://gitee.com/edgegallery/community/blob/master/Architecture%20WG/Requirements/v0.9/network_isolation.md) | 高维涛|
| Public Cloud Integration/公有云集成 |  [PublicCloudIntegration_EPIC](https://gitee.com/OSDT/dashboard/programs/114633/issues?issue_id=I1OXZF) |  [public cloud integration design doc](https://gitee.com/edgegallery/community/blob/master/Architecture%20WG/Requirements/v0.9/publiccloud_integration.md)  |    |
| DNS Configuration/支持DNS配置 | [DNS_Configuration_EPIC](https://gitee.com/OSDT/dashboard/programs/114633/issues?issue_id=I1OWJI) | [DNS Configuration Design doc](https://gitee.com/edgegallery/community/blob/master/Architecture%20WG/Requirements/v0.9/open_dns_configuration.md) | Libu |

