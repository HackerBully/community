## 工作组目标

架构工作组在 [社区技术章程](https://gitee.com/EdgeGallery/community/wikis/Technical%20Charter?sort_id=2424069)的指导下，构建富有竞争力的边缘计算架构，释放5G 网络能力，简化应用开发者上车难度。

## 工作组主要职责

架构工作组负责 EdgeGallery 架构的演进与维护，同时也需要拉通各个子项目之间的整体架构设计。

## 工作组预期成果

- 架构工作组将制定并维护EdgeGallery的架构设计（如部署、安全、功能等）
- 架构工作组将定期或者在版本发布过程中安排每个项目组的架构陈述，以便了解所有项目组之间的API交互以及通信。
- 架构工作组将分析需求（来源于TSC/工作组/项目组 等）并落地到对应项目组
- 架构工作组将定期向TSC汇报架构的更新

## 工作组成员
| **Name**          | **Affiliation**       | **Email**                                                   |  **Self nominate as Chair (Y/N)** | **Self Nominate as Co-Chair (Y/N)** |
|-------------------|-----------------------|-------------------------------------------------------------|--------------------------------|-------------------------------------|
| 游永明     | 广州申迪           | youym@189.cn     |                               |                                    
| 高维涛 | 华为 |  victor.gao@huawei.com  |  Y  | -  |
| 刘小飞 | 上海讯琥 |  xiaofei.liu@xeniro.io  | -   | -  |
| Satish Karunanithi | Huawei |  satishk@huawei.com  | -   | -  |
| Libu Jacob Varghese | Huawei | libu.jacob@huawei.com  | -   | -  |
| 范桂飓 | 九州云 | fan.guiju@99cloud.net  | -   | -  |
| 刘辉 | 紫金山实验室 | liuhui@pmlabs.com.cn  | -   | -  |
| 章清洁 | 华为 | zhangqingjie@huawei.com | - | - |

## 工作组会议
* 补充会议信息

## 工作组联系方式
* 邮件列表：架构工作组使用Edge Gallery邮件列表： main@edgegallery.groups.io。
* 其他： 请在邮件标题添加【ARC】用以区分
