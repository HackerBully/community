## 工作组目标
* 围绕MEC的应用，以Edge Gallery为平台，搭建不同场景的实验室环境，方便开发验证、对外演示，快速发展MEC应用生态。

## 工作组范围
* 负责为社区提供Edge Gallery的验证、演示实验室环境，提供Edge Gallery的CI/CD环境；

## 工作组PTL
* 刘芷若 liuzhiruo@caict.ac.cn

## 工作组成员
|   **Name**          | **Affiliation**       | **Email**                                                   |  **Self nominate as Chair (Y)** | **Self Nominate as Co-Chair (Y/N)** |
|-----------------------|-----------------------|-------------------------------------------------------------|--------------------------------|-------------------------------------|
| 刘芷若     | 信通院   |  liuzhiruo@caict.ac.cn   | Y  |   |
| 苏锦烽     | 信通院   |  sujinfeng@bifnc.cn   |   |   |
| 徐舒      | 联通    | xus15@chinaunicom.cn |                          |                                     |
| 褚纬明     | 安恒           | weiming.chu@dbappsecurity.com.cn     |                               |                                     |
| 姜伟       | 紫金山  | jiangwei@pmlabs.com.cn  |                               |                                     |
| 丁宇卿       | 紫金山  | dingyuqing@pmlabs.com.cn  |                               |                                     |
| 侯敏熙      | 联想  | 18515041014@163.com|                               |                                     |
| 周俊      | 华为  | zhoujun8@huawei.com |                               |                                     |
|丁智慧    |  云迅智能  | dingzhihui@yunex.com  |  |  |
|彭育    |  华为 | perry.peng@huawei.com  |  |  |
## 工作组会议
* 每周例会时间定在：每周二下午16:00-17:00，  每周五下班前在wiki上申报议题。

* 第一次集成组例会：
会议时间：2020年7月21日16:00-17:00
会议链接：https://welink-meeting.zoom.us/j/989530379
会议议题：各方提诉求，讨论明确Edge Gallery集成组工作范围和目标

## 工作组联系方式
* 邮件列表：main@edgegallery.groups.io

## 技术文档归档路径
- https://gitee.com/-/ide/project/edgegallery/community/edit/master/-/Integration%20WG/Introduction.md