## 项目目标
* 拉通各开发项目组，对需求进行详细方案设计，确保规划需求可以有效的落地
* 通过定期的技术方案讨论 和 网络安全研讨，打造安全可靠的EdgeGallery平台

## 项目范围
* 拉通Developer、App Store、User-mgmt的需求设计，保障版本的落地开发
* 遗留问题跟踪和闭环
* 提供技术方案求助通道，解决开发过程中遇到的问题

## 项目成员
| **Name**  | **Affiliation** |         **Email**         |  **Self nominate as Chair (Y)** | **Self Nominate as Co-Chair (Y/N)** |
|-----------|-----------------|---------------------------|---------------------------------|-------------------------------------|
| 毛健      | 赛特斯           | maoj@certusnet.com.cn     |                                 |                                     
| 王立中    | 赛特斯           | wanglz@certusnet.com.cn   |                                 |                                     
| 马伟      | 赛特斯           | mawei@certusnet.com.cn    |                                 |                                     
| 张倍源      | 华为           | zhangbeiyuan@huawei.com    |                                 |                                     
| 孙靖涵    | 华为             | sunjinghan1@huawei.com    |                                 |                                     
| 李治谦    | 华为             | lizhiqian1@huawei.com     |                                 |   
| 杨阳    | 华为             | yangyang263@huawei.com   |                                 |   
| 李松阳    | 华为             | lisongyang2@huawei.com   |                                 |   
| 贺龙飞    | 华为             | helongfei6@huawei.com   |                                 |   
| 张海龙    | 华为             | zhanghailong22@huawei.com   |                                 |   
| 白针针    | 华为             | baizhenzhen3@huawei.com   |                                 |   
| 邹玲莉    | 华为             | zoulingli@huawei.com   |                                 |   

## 项目会议
* 每周一 16:30-17:30
* 会议链接：https://welink-meeting.zoom.us/j/531038090

## 会议纪律
* 1 会前需要做充足的准备，输出方案的详细设计，线下已经沟通过，线上更加关注方案的可行性和实现落地；
* 2 会议时间需要严格守时，开一个高效的会议

## 项目联系方式
* 邮件列表：使用Edge Gallery邮件列表： main@edgegallery.groups.io。
* 其他： 请在邮件标题添加【App&Dev Joint PT】用以区分
** 