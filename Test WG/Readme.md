## 工作组目标
* 负责社区的集成测试，以及Usecase集成测试，对所有涉及的端到端业务验证负责，构建社区分层测试体系，以及社区测试方案，流程和测试目标，支撑开源EdgeGallery高质量版本发布和演进；社区CI/CD环境支撑和维护；
## 工作组主要职责
* 社区分层自动化测试体系构建和演进；
* 社区测试方案，策略以及测试目标设计和规划；
* 端到端社区Usecase集成和验证；
* 社区CI/CD环境支撑和维护；
## 工作组成员
| **Name**          | **Affiliation**       | **Email**                                                   |  **Self nominate as Chair (Y)** | **Self Nominate as Co-Chair (Y/N)** |
|-------------------|-----------------------|-------------------------------------------------------------|--------------------------------|-------------------------------------|
| 彭育     | 华为           | perry.peng@huawei.com     | Y                              |                                     |
| 侯敏熙     | 联想           | 18515041014@163.com     | Y                              |                                     |
| 姜伟     | 紫金山实验室           | jiangwei@pmlabs.com.cn     |                               |                                     
| 刘辉     | 紫金山实验室           | liuhui@pmlabs.com.cn     | Y                              |      
| 张怡     | 联想                  | zhangyil@lenovo.com       |                               |
| 张阿利     | 中软                  | a_lizhang@163.com       |                               |
| 李媛媛     | 中软                  | jasmine-Lyy@outlook.com       |                               |  
| 孟璞辉     | VMware                  | pmeng@vmware.com       |                               |  

## 工作组会议
* 每周四晚上7-8点周例会；
* 每周二前在meeting下反馈下次例会议题；

## 工作组repo
* 待补充
## 工作组联系方式
* 邮件列表：测试工作组使用Edge Gallery邮件列表： main@edgegallery.groups.io。
* 其他： 请在邮件标题添加【TEST】用以区分
